# Movie app

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).\
To be able to run the app, perform the following steps:
1. Clone the repository to your machine:\
`git clone git@gitlab.com:ali.a.rustamov/movie-app.git`\
or\
`git clone https://gitlab.com/ali.a.rustamov/movie-app.git`
2. run `yarn` in the project folder

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `yarn storybook`

Runs the Storybook locally and output the address. Depending on your system configuration, it will automatically open the address in a new browser tab.
