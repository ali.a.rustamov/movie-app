import { Typography } from 'components/atoms/Typography';
import parse from 'html-react-parser';
import React from 'react';
import styled from 'styled-components';

import { Props } from './props';

const ShowInfoBase = styled.div`
  & .show-block {
    display: flex;
    flex-wrap: wrap;
    margin: 0 -5px;
    & .show-image,
    .show-summary,
    .show-info-card {
      padding: 0 5px 0 5px;
    }
    & .show-image {
      width: calc(100% / 6 - 10px);
      & img {
        width: 100%;
      }
    }
    & .show-summary {
      width: calc(100% / 2 - 10px);
      text-align: left;
      text-indent: 20px;
      p {
        padding-left: 10px;
        padding-right: 10px;
      }
    }
    & .show-info-card {
      width: calc(100% / 3 - 10px);
      background-color: #eee;
      box-shadow: 0 1px 5px 0 rgba(0, 0, 0, 0.15);
      padding: 10px;
      box-sizing: border-box;
      & .star {
        width: 15px;
      }
    }
  }
  & .show-name {
    margin-left: 0px;
    padding-bottom: 5px;
  }
`;

export const ShowInfo: React.FC<Props> = ({ show, ...rest }) => (
  <ShowInfoBase {...rest}>
    <Typography className="show-name" variant="movieTitle" color="black">
      {show.name}
    </Typography>
    <div className="show-block">
      <div className="show-image">
        <img src={show.image?.medium} alt={show.name} />
      </div>
      <div className="show-summary">
        <Typography variant="text" color="black">
          {parse(show.summary || '')}
        </Typography>
      </div>
      <div className="show-info-card">
        <Typography variant="movieTitle" color="black">
          Show Info
        </Typography>
        <br />
        <Typography variant="cardTitle" color="black" bold>
          Network:&nbsp;
        </Typography>
        <Typography variant="text" color="black">
          {show.network?.name}
        </Typography>
        <br />
        <Typography variant="cardTitle" color="black" bold>
          Schedule:&nbsp;
        </Typography>
        <Typography variant="text" color="black">
          {show.schedule?.days.join(', ')} at {show.schedule?.time}
        </Typography>
        <br />
        <Typography variant="cardTitle" color="black" bold>
          Status:&nbsp;
        </Typography>
        <Typography variant="text" color="black">
          {show.status}
        </Typography>
        <br />
        <Typography variant="cardTitle" color="black" bold>
          Show Type:&nbsp;
        </Typography>
        <Typography variant="text" color="black">
          {show.type}
        </Typography>
        <br />
        <Typography variant="cardTitle" color="black" bold>
          Genres:&nbsp;
        </Typography>
        <Typography variant="text" color="black">
          {show.genres?.join(', ')}
        </Typography>
        <br />
        <Typography variant="cardTitle" color="black" bold>
          Official site:&nbsp;
        </Typography>
        <Typography variant="link" color="link">
          <a href={show.officialSite} target="_blank">
            {show.officialSite}
          </a>
        </Typography>
        <br />
        <Typography variant="cardTitle" color="black" bold>
          Rating:&nbsp;
        </Typography>
        <Typography variant="text" color="black">
          <img className="star" src="/star.svg" /> {show.rating?.average}
        </Typography>
      </div>
    </div>
  </ShowInfoBase>
);
