import { HTMLAttributes } from 'react';
import { Show } from 'types/Show';

export type Props = HTMLAttributes<HTMLDivElement> & {
  readonly show: Show;
};
