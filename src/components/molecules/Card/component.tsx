import { Typography } from 'components/atoms/Typography';
import React from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

import { Props } from './props';

const MovieCardContainer = styled.a`
  padding: 0 5px 10px 5px;
  width: calc(100% / 6 - 10px);
  margin-bottom: 10px;
  text-decoration: none;
  & :hover {
    opacity: 0.9;
  }
`;

const MovieCard = styled.div`
  background: #1a1a1a;
`;

const Poster = styled.div<Pick<Props['show'], 'image'>>`
  ${(props) => `background: url("${props?.image?.medium}");`}
  background-repeat: no-repeat;
  background-position: top;
  width: 100%;
  height: 225px;
  margin-bottom: 5px;
`;

const InfoWrapper = styled.div`
  padding: 5px 5px 20px 5px;
  & .star {
    width: 15px;
  }
  & .cardTitle {
    padding-top: 10px;
  }
`;

export const Card: React.FC<Props> = ({ show, ...rest }) => (
  <MovieCardContainer as={Link} {...rest}>
    <MovieCard>
      <Poster image={show.image} />
      <InfoWrapper>
        <img className="star" src="/star.svg" alt="star" />{' '}
        <Typography variant="rating" color="silver">
          {show.rating?.average}
        </Typography>
        <br />
        <Typography variant="cardTitle" color="white" className="cardTitle">
          {show.name}
        </Typography>
      </InfoWrapper>
    </MovieCard>
  </MovieCardContainer>
);
