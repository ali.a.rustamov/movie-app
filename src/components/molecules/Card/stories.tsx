import { Meta, Story } from '@storybook/react/types-6-0';
import React from 'react';

import { Card } from './component';
import { Props as CardProps } from './props';

export default {
  title: 'Components/Card',
  component: Card,
} as Meta;

const Template: Story<CardProps> = (args) => <Card {...args} />;

export const Default = Template.bind({});
// eslint-disable-next-line functional/immutable-data
Default.args = {
  show: {
    id: 66,
    url: 'http://www.tvmaze.com/shows/66/the-big-bang-theory',
    image: {
      medium: 'http://static.tvmaze.com/uploads/images/medium_portrait/173/433868.jpg',
    },
    name: 'The Big Bang Theory',
    rating: {
      average: 7.9,
    },
  },
};
