import { HTMLAttributes } from 'react';
import { LinkProps } from 'react-router-dom';
import { Show } from 'types/Show';

export type Props = LinkProps & {
  readonly show: Show;
};
