import React from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

import { Props } from './props';

const NavbarBase = styled.div`
  height: 46px;
  border-bottom: 1px solid #000;
  margin-bottom: 10px;

  ul {
    list-style-type: none;
    margin: 0;
    padding: 0;
  }
  & li {
    display: inline;
    float: left;

    a {
      display: block;
      width: 60px;
      background-color: #000;
      padding: 14px 16px;
      text-decoration: none;
      color: #fff;

      &:hover {
        opacity: 0.5;
      }
    }
  }
`;

export const Navbar: React.FC<Props> = ({ routes }) => (
  <NavbarBase>
    <ul>
      {routes.map((r, i) => (
        <li key={i}>
          <Link to={r.path}>{r.label}</Link>
        </li>
      ))}
    </ul>
  </NavbarBase>
);
