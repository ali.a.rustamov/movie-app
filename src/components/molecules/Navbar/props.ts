import { HTMLAttributes } from 'react';
import { Route } from 'router/MainRouter/types/Route';

export type Props = HTMLAttributes<HTMLDivElement> & {
  readonly routes: ReadonlyArray<Route>;
};
