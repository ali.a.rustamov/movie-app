import { Meta, Story } from '@storybook/react/types-6-0';
import { Card } from 'components/molecules/Card';
import React from 'react';

import { CardGroup } from './component';
import { Props as CardGroupProps } from './props';

export default {
  title: 'Components/CardGroup',
  component: CardGroup,
} as Meta;

const shows = [
  {
    id: 66,
    url: 'http://www.tvmaze.com/shows/66/the-big-bang-theory',
    image: {
      medium: 'http://static.tvmaze.com/uploads/images/medium_portrait/173/433868.jpg',
    },
    name: 'The Big Bang Theory',
    rating: {
      average: 7.9,
    },
  },
  {
    id: 66,
    url: 'http://www.tvmaze.com/shows/66/the-big-bang-theory',
    image: {
      medium: 'http://static.tvmaze.com/uploads/images/medium_portrait/173/433868.jpg',
    },
    name: 'The Big Bang Theory',
    rating: {
      average: 7.9,
    },
  },
  {
    id: 66,
    url: 'http://www.tvmaze.com/shows/66/the-big-bang-theory',
    image: {
      medium: 'http://static.tvmaze.com/uploads/images/medium_portrait/173/433868.jpg',
    },
    name: 'The Big Bang Theory',
    rating: {
      average: 7.9,
    },
  },
  {
    id: 66,
    url: 'http://www.tvmaze.com/shows/66/the-big-bang-theory',
    image: {
      medium: 'http://static.tvmaze.com/uploads/images/medium_portrait/173/433868.jpg',
    },
    name: 'The Big Bang Theory',
    rating: {
      average: 7.9,
    },
  },
  {
    id: 66,
    url: 'http://www.tvmaze.com/shows/66/the-big-bang-theory',
    image: {
      medium: 'http://static.tvmaze.com/uploads/images/medium_portrait/173/433868.jpg',
    },
    name: 'The Big Bang Theory',
    rating: {
      average: 7.9,
    },
  },
  {
    id: 66,
    url: 'http://www.tvmaze.com/shows/66/the-big-bang-theory',
    image: {
      medium: 'http://static.tvmaze.com/uploads/images/medium_portrait/173/433868.jpg',
    },
    name: 'The Big Bang Theory',
    rating: {
      average: 7.9,
    },
  },
  {
    id: 66,
    url: 'http://www.tvmaze.com/shows/66/the-big-bang-theory',
    image: {
      medium: 'http://static.tvmaze.com/uploads/images/medium_portrait/173/433868.jpg',
    },
    name: 'The Big Bang Theory',
    rating: {
      average: 7.9,
    },
  },
  {
    id: 66,
    url: 'http://www.tvmaze.com/shows/66/the-big-bang-theory',
    image: {
      medium: 'http://static.tvmaze.com/uploads/images/medium_portrait/173/433868.jpg',
    },
    name: 'The Big Bang Theory',
    rating: {
      average: 7.9,
    },
  },
  {
    id: 66,
    url: 'http://www.tvmaze.com/shows/66/the-big-bang-theory',
    image: {
      medium: 'http://static.tvmaze.com/uploads/images/medium_portrait/173/433868.jpg',
    },
    name: 'The Big Bang Theory',
    rating: {
      average: 7.9,
    },
  },
];

const Template: Story<CardGroupProps> = () => (
  <CardGroup>{shows && shows.map((show, i) => <Card to="#" show={show} key={i} />)}</CardGroup>
);

export const Default = Template.bind({});
