import React from 'react';
import styled from 'styled-components';

import { Props } from './props';

const CardGroupBase = styled.div`
  display: flex;
  flex-wrap: wrap;
  margin: 0 -5px;
`;

export const CardGroup: React.FC<Props> = ({ children }) => (
  <CardGroupBase>{children}</CardGroupBase>
);
