import { showsListSearchGet } from 'api/shows';
import { TextInput } from 'components/atoms/TextInput';
import { Typography } from 'components/atoms/Typography';
import { Card } from 'components/molecules/Card';
import { CardGroup } from 'components/organisms/CardGroup';
import { useDebounce } from 'hooks/useDebounce';
import React, { FormEvent, useCallback, useEffect, useState } from 'react';
import { Show } from 'types/Show';

import { Props } from './props';

export const Search: React.FC<Props> = ({ ...rest }) => {
  const [shows, setShows] = useState<ReadonlyArray<Show>>([]);
  const [searchTerm, setSearchTerm] = useState('');
  const debouncedSearchTerm = useDebounce(searchTerm, 500);

  const getShowsList = useCallback(async () => {
    const showsResult = await showsListSearchGet(debouncedSearchTerm);
    setShows(showsResult.map((s) => s.show));
  }, [debouncedSearchTerm]);

  useEffect(() => {
    getShowsList();
  }, [getShowsList]);

  const handleTextInputChange = useCallback((e: FormEvent<HTMLInputElement>) => {
    setSearchTerm(e.currentTarget.value);
  }, []);

  return (
    <div {...rest}>
      <Typography variant="movieTitle" color="black">
        Search shows
      </Typography>
      <TextInput placeholder="Start typing the show name..." onChange={handleTextInputChange} />
      {shows.length === 0 && debouncedSearchTerm.length > 0 ? (
        <>
          <br />
          <Typography variant="cardTitle" color="black">
            Pardon us, but no shows or people matching your query were found.
          </Typography>
        </>
      ) : (
        <CardGroup>
          {shows.map((show, i) => (
            <Card show={show} to={`/movie/${show.id}`} key={i} />
          ))}
        </CardGroup>
      )}
    </div>
  );
};
