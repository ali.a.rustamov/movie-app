import { showsListGet } from 'api/shows';
import { Pagination } from 'components/atoms/Pagination';
import { Typography } from 'components/atoms/Typography';
import { Card } from 'components/molecules/Card';
import { CardGroup } from 'components/organisms/CardGroup';
import React, { useCallback, useEffect, useState } from 'react';
import { Show } from 'types/Show';

import { Props } from './props';

export const Home: React.FC<Props> = ({ ...rest }: Props) => {
  const [page, setPage] = useState<number>(0);
  const [pageCount, setPageCount] = useState<number>(2);
  const [shows, setShows] = useState<ReadonlyArray<Show>>([]);

  const getShowsList = useCallback(async () => {
    const showsResult = await showsListGet(page);
    if (showsResult.length > 0) {
      setPageCount((p) => (page + 2 > p ? page + 2 : p));
    } else {
      setPageCount((p) => p - 1);
    }
    setShows(showsResult);
  }, [page]);

  useEffect(() => {
    getShowsList();
  }, [getShowsList]);

  const handlePageChange = useCallback((e: { readonly selected: number }) => {
    setPage(e.selected);
  }, []);

  return (
    <div {...rest}>
      <Typography variant="movieTitle" color="black">
        TVmaze shows
      </Typography>
      <Pagination
        pageCount={pageCount}
        pageRangeDisplayed={3}
        marginPagesDisplayed={1}
        initialPage={page}
        forcePage={page}
        onPageChange={handlePageChange}
      />
      <CardGroup>
        {shows && shows.map((show, i) => <Card show={show} to={`/movie/${show.id}`} key={i} />)}
      </CardGroup>
      <Pagination
        pageCount={pageCount}
        pageRangeDisplayed={3}
        marginPagesDisplayed={1}
        initialPage={page}
        forcePage={page}
        onPageChange={handlePageChange}
      />
    </div>
  );
};
