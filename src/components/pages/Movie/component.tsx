import { showGet, showsListGet } from 'api/shows';
import { ShowInfo } from 'components/molecules/ShowInfo';
import React, { useCallback, useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { Show } from 'types/Show';

import { Props } from './props';

export const Movie: React.FC<Props> = ({ ...rest }) => {
  const { id } = useParams<{ readonly id: string }>();
  const [show, setShow] = useState<Show>();

  const getShow = useCallback(async () => {
    const showResult = await showGet(id);
    setShow(showResult);
  }, [id]);

  useEffect(() => {
    getShow();
  }, [getShow]);

  return <div {...rest}>{show && <ShowInfo show={show} />}</div>;
};
