import { createElement } from 'react';
import styled from 'styled-components';

import { Props } from './props';
import { variantElements } from './variantElements';

const TypographyBase: React.FC<Props> = ({ variant, bold, children, ...rest }) =>
  createElement(variantElements[variant], { ...rest }, children);

export const Typography = styled(TypographyBase)`
  color: ${(props) =>
    (props.color === 'white' && '#fff') ||
    (props.color === 'black' && '#000') ||
    (props.color === 'silver' && 'silver') ||
    (props.color === 'link' && '#136CB2')};
  font-size: ${(props) =>
    (props.variant === 'text' && '14px') ||
    (props.variant === 'cardTitle' && '16px') ||
    (props.variant === 'movieTitle' && '20px') ||
    (props.variant === 'rating' && '20px') ||
    (props.variant === 'link' && '12px')};
  font-family: Roboto, Helvetica, Arial, sans-serif;
  ${(props) => props.bold && `font-weight: bold;`}
`;
