export const variantElements = {
  text: 'span',
  cardTitle: 'h2',
  movieTitle: 'h1',
  rating: 'span',
  link: 'a',
};
