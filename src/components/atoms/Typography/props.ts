export type Props = {
  readonly variant: 'text' | 'cardTitle' | 'movieTitle' | 'rating' | 'link';
  readonly color: 'white' | 'black' | 'silver' | 'link';
  readonly bold?: boolean;
  readonly className?: string;
};
