import { ReactPaginateProps } from 'react-paginate';

export type Props = ReactPaginateProps;
