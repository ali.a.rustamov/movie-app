import React from 'react';
import ReactPaginate from 'react-paginate';
import styled from 'styled-components';

import { Props } from './props';

const PaginationWrapper = styled.div`
  list-style-type: none;
  display: inline-block;
  text-align: center;
  margin-top: 0;
  margin-bottom: 50px;
  & li {
    display: inline;
    float: left;
    padding: 8px 16px;
    text-decoration: none;
    cursor: pointer;
    a {
      color: black;
      outline: none;
      &:hover {
        color: #888;
      }
    }
  }
  & .selected {
    background-color: #e5e5e5;
  }
`;

export const Pagination: React.FC<Props> = ({ ...rest }) => (
  <PaginationWrapper>
    <ReactPaginate {...rest} />
  </PaginationWrapper>
);
