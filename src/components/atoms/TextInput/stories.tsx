import { Meta, Story } from '@storybook/react/types-6-0';
import React from 'react';

import { TextInput } from './component';
import { Props } from './props';

export default {
  title: 'Components/TextInput',
  component: TextInput,
} as Meta;

const Template: Story<Props> = (args) => <TextInput {...args} />;

export const Default = Template.bind({});
