import React from 'react';
import styled from 'styled-components';

import { Props } from './props';

const TextInputBase = styled.input`
  font-family: Roboto, Helvetica, Arial, sans-serif;
  color: #333;
  font-size: 16px;
  margin: 0 auto;
  padding: 15px;
  border-radius: 2px;
  background-color: rgb(255, 255, 255);
  border: 1px solid #aaa;
  width: 90%;
  display: block;
  transition: all 0.3s;
  margin-bottom: 15px;
`;

export const TextInput: React.FC<Props> = ({ ...rest }: Props) => (
  <TextInputBase type="text" {...rest} />
);
