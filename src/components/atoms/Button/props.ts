import { HTMLAttributes } from 'react';

export type Props = HTMLAttributes<HTMLButtonElement> & {
  readonly color?: 'default' | 'primary' | 'danger';
};
