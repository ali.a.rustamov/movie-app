import { Meta, Story } from '@storybook/react/types-6-0';
import React from 'react';

import { Button } from './component';
import { Props as ButtonProps } from './props';

export default {
  title: 'Components/Button',
  component: Button,
} as Meta;

const Template: Story<ButtonProps> = (args) => <Button {...args} />;

export const Default = Template.bind({});
// eslint-disable-next-line functional/immutable-data
Default.args = {
  color: 'primary',
  children: 'Button',
};
