import React from 'react';
import styled from 'styled-components';

import { Props } from './props';

const StyledButton = styled.button`
  background-color: #f5c518;
  border-radius: 4px;
  border: none;
  color: #000;
  padding: 0 2rem;
  padding-top: 0px;
  padding-right: 2rem;
  padding-bottom: 0px;
  padding-left: 2rem;
  min-height: 2.25rem;
  font-size: 0.875rem;
`;

export const Button: React.FC<Props> = ({ children }) => <StyledButton>{children}</StyledButton>;
