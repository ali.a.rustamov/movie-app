import axios from 'axios';
import { Show } from 'types/Show';

export const showsListGet = async (page = 0): Promise<ReadonlyArray<Show>> => {
  const result = await axios.get(`http://api.tvmaze.com/shows?page=${page}`);
  return result.data;
};

export const showGet = async (id: string): Promise<Show> => {
  const result = await axios.get(`http://api.tvmaze.com/shows/${id}`);
  return result.data;
};

export const showsListSearchGet = async (
  searchTerm: string,
): Promise<ReadonlyArray<{ readonly score: number; readonly show: Show }>> => {
  const result = await axios.get(`http://api.tvmaze.com/search/shows?q=${searchTerm}`);
  return result.data;
};
