import './App.css';

import { MainTemplate } from 'components/templates/MainTemplate';
import React from 'react';
import { MainRouter } from 'router/MainRouter';

const App: React.FC = () => {
  return (
    <div className="App">
      <MainTemplate>
        <MainRouter />
      </MainTemplate>
    </div>
  );
};

export default App;
