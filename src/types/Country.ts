export type Country = {
  readonly name: string;
  readonly code: string;
  readonly timezone: string;
};
