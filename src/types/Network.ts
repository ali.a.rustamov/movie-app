import { Country } from './Country';

export type Network = {
  readonly id: number;
  readonly name: string;
  readonly country: Country;
};
