export type Schedule = {
  readonly time: string;
  readonly days: ReadonlyArray<string>;
};
