export type Externals = {
  readonly tvrage: number;
  readonly thetvdb: number;
  readonly imdb: string;
};
