import { Externals } from './Externals';
import { Image } from './Image';
import { Network } from './Network';
import { Schedule } from './Schedule';

export type Show = {
  readonly id: number;
  readonly url: string;
  readonly name: string;
  readonly type?: string;
  readonly language?: string;
  readonly genres?: ReadonlyArray<string>;
  readonly status?: string;
  readonly runtime?: number;
  readonly premiered?: string;
  readonly officialSite?: string;
  readonly schedule?: Schedule;
  readonly rating?: {
    readonly average?: number;
  };
  readonly weight?: number;
  readonly network?: Network;
  readonly webChannel?: string | null;
  readonly externals?: Externals;
  readonly image?: Image;
  readonly summary?: string;
  readonly updated?: number;
};
