export type Image = {
  readonly medium?: string;
  readonly original?: string;
};
