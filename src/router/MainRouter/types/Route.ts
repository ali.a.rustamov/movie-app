import React from 'react';

export type Route = {
  readonly path: string;
  readonly component: React.ComponentType;
  readonly label?: string;
  readonly exact?: boolean;
};
