import { Navbar } from 'components/molecules/Navbar';
import React from 'react';
import { BrowserRouter as Router, Link, Route, Switch } from 'react-router-dom';

import { routes } from './routes';

export const MainRouter: React.FC = () => (
  <Router>
    <div>
      <Navbar routes={[routes[0], routes[2]]} />
      <Switch>
        {routes.map((route, index) => (
          <Route exact={route.exact} path={route.path} component={route.component} key={index} />
        ))}
      </Switch>
    </div>
  </Router>
);
