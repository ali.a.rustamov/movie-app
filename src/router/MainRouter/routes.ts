import { Home } from 'components/pages/Home';
import { Movie } from 'components/pages/Movie';
import { Search } from 'components/pages/Search';

import { Route } from './types/Route';

export const routes: ReadonlyArray<Route> = [
  {
    path: '/',
    label: 'Home',
    exact: true,
    component: Home,
  },
  {
    path: '/movie/:id',
    label: 'Movie',
    component: Movie,
  },
  {
    path: '/search',
    label: 'Search',
    component: Search,
  },
];
